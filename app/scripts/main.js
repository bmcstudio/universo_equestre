(function(window, history, undefined) {

	"use strict";

    var location = window.history.location || window.location;

    // Bind to StateChange Event
    // History.Adapter.bind(window, 'statechange', function() { // Note: We are using statechange instead of popstate
    window.addEventListener("popstate", function() { // Note: We are using statechange instead of popstate
        // changePage(location.pathname); // Note: We are using History.getState() instead of event.state

        if (location.pathname.split("/").pop() != '') {
            var prevUrl = 'pages/'+location.pathname.split("/").pop();
            changePage(prevUrl);
        } else {
            changePage(location.pathname  + 'pages/home.html');
        }
    });

    document.addEventListener('DOMContentLoaded', function () {
    	changePage(location.pathname + 'pages/home.html');
    });

    var links = document.querySelectorAll('.link');
    // [].slice.call( links ).forEach( function( bttn ) {
    Array.prototype.forEach.call(links, function(elem, id) {
        elem.addEventListener('click', function(e) {
            changePage(elem.href);
            history.pushState(null, null, elem.href.split("/").pop());
            e.preventDefault();
        }, false);
    });

    function addHandleLink() {
        window.scrollTo(0, 0);

        var insideLinks = document.querySelectorAll('#main .link');
        Array.prototype.forEach.call(insideLinks, function(elem, id) {
            elem.addEventListener('click', function(e) {
                changePage(elem.href);
                history.pushState(null, null, elem.href.split("/").pop());
                e.preventDefault();
            }, false);
        });
    }

    function changePage(href) {

        $.ajax({url: href}).done(successCallback).fail(errorCallback);

        function successCallback (data) {
            $('#main').html(data);
            addHandleLink();
        }

        function errorCallback (data) {
            console.log('deu erro dos brabo!');
        }
    }

})(window, window.history);